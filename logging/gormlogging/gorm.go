package gormlogging

import (
	"context"
	"errors"
	"fmt"
	"os"
	"path/filepath"
	"runtime"
	"strings"
	"time"

	"go.uber.org/zap"
	"go.uber.org/zap/zapcore"
	"gorm.io/gorm"
	"gorm.io/gorm/logger"
)

var (
	gormPackage    = "gorm.io"
	goUtilsPackage = filepath.Join("go-utils", "logging", "gorm_logging")
)

// GormLogger is the interface for gorm logger.
type GormLogger interface {
	logger.Interface
	SetAsDefault()
	SetSlowThreshold(duration time.Duration)
	SetIgnoreRecordNotFoundError(ignore bool)
}

type gormLogger struct {
	zapLogger                 *zap.Logger
	logLevel                  logger.LogLevel
	slowThreshold             time.Duration
	ignoreRecordNotFoundError bool
}

// NewGormLogger creates a new gorm logger instance.
func NewGormLogger(levelStr string) (GormLogger, error) {
	logLevel := logger.Error
	if levelStr != "" {
		var err error
		logLevel, err = parserLevel(levelStr)
		if err != nil {
			return nil, err
		}
	}

	// Define a custom time format.
	encoderCfg := zap.NewProductionEncoderConfig()
	encoderCfg.EncodeTime = func(t time.Time, enc zapcore.PrimitiveArrayEncoder) {
		enc.AppendString(t.UTC().Format(time.RFC3339))
	}

	atomInstance := zap.NewAtomicLevelAt(zap.DebugLevel)
	loggerInstance := zap.New(zapcore.NewCore(
		zapcore.NewJSONEncoder(encoderCfg),
		zapcore.Lock(os.Stdout),
		atomInstance,
	), zap.AddStacktrace(zap.PanicLevel), zap.WithCaller(true))

	return &gormLogger{
		zapLogger:                 loggerInstance,
		logLevel:                  logLevel,
		slowThreshold:             500 * time.Millisecond,
		ignoreRecordNotFoundError: true,
	}, nil
}

func (l *gormLogger) SetAsDefault() {
	logger.Default = l
}

func (l *gormLogger) SetSlowThreshold(duration time.Duration) {
	l.slowThreshold = duration
}

func (l *gormLogger) SetIgnoreRecordNotFoundError(ignore bool) {
	l.ignoreRecordNotFoundError = ignore
}

func (l *gormLogger) LogMode(level logger.LogLevel) logger.Interface {
	return &gormLogger{
		zapLogger:                 l.zapLogger,
		slowThreshold:             l.slowThreshold,
		logLevel:                  level,
		ignoreRecordNotFoundError: l.ignoreRecordNotFoundError,
	}
}

func (l *gormLogger) Info(ctx context.Context, str string, args ...interface{}) {
	if l.logLevel < logger.Info {
		return
	}
	l.logger().Sugar().Debugf(str, args...)
}

func (l *gormLogger) Warn(ctx context.Context, str string, args ...interface{}) {
	if l.logLevel < logger.Warn {
		return
	}
	l.logger().Sugar().Warnf(str, args...)
}

func (l *gormLogger) Error(ctx context.Context, str string, args ...interface{}) {
	if l.logLevel < logger.Error {
		return
	}
	l.logger().Sugar().Errorf(str, args...)
}

func (l *gormLogger) Trace(ctx context.Context, begin time.Time, fc func() (string, int64), err error) {
	if l.logLevel <= logger.Silent {
		return
	}
	elapsed := time.Since(begin)
	zapLogger := l.logger()
	switch {
	case err != nil && l.logLevel >= logger.Error && (!l.ignoreRecordNotFoundError || !errors.Is(err, gorm.ErrRecordNotFound)):
		sql, rows := fc()
		zapLogger.Error("trace", zap.Error(err), zap.Duration("elapsed", elapsed), zap.Int64("rows", rows), zap.String("sql", sql))
	case l.slowThreshold != 0 && elapsed > l.slowThreshold && l.logLevel >= logger.Warn:
		sql, rows := fc()
		zapLogger.Warn("trace", zap.Duration("elapsed", elapsed), zap.Int64("rows", rows), zap.String("sql", sql))
	case l.logLevel >= logger.Info:
		sql, rows := fc()
		zapLogger.Debug("trace", zap.Duration("elapsed", elapsed), zap.Int64("rows", rows), zap.String("sql", sql))
	}
}

func (l *gormLogger) logger() *zap.Logger {
	zapLogger := l.zapLogger

	for i := 2; i < 15; i++ {
		_, file, _, ok := runtime.Caller(i)
		switch {
		case !ok:
		case strings.Contains(file, gormPackage):
		case strings.Contains(file, goUtilsPackage):
		default:
			return zapLogger.WithOptions(zap.AddCallerSkip(i - 1))
		}
	}
	return zapLogger
}

func parserLevel(levelStr string) (logger.LogLevel, error) {
	levelStr = strings.ToLower(levelStr)
	switch levelStr {
	case "info":
		fallthrough
	case "debug":
		return logger.Info, nil
	case "warn":
		return logger.Warn, nil
	case "fatal":
		return logger.Silent, nil
	case "panic":
		return logger.Silent, nil
	case "error":
		return logger.Error, nil
	}

	return 0, fmt.Errorf("unknown log level: %s", levelStr)
}
