package logging

type Logging interface {
	Info(message string)
	Debug(message string)
	Warn(message string)
	Error(message string)
	Panic(message string)
	Fatal(message string)
}
