# GO UTILS ECHO LOGGING MIDDLEWARE

This package implements a custom echo logger middleware that uses zap logging internally.

## Log Levels

```text
debug -> info -> warn -> error -> panic -> fatal
```

## Example

```go
package main

import (
	"github.com/labstack/echo/v4"
	"gitlab.com/bobble-public/backend/go-utils/logging"
	"gitlab.com/bobble-public/backend/go-utils/logging/echo_logging"
	"math/rand"
	"net/http"
	"time"
)

func main() {
	echoLogger, err := echo_logging.NewEchoMiddleware("error")
	if err != nil {
		logging.Panic(err.Error())
	}

	e := echo.New()
	e.HideBanner = true
	e.HidePort = true
	e.Use(echoLogger)

	e.GET("/", func(c echo.Context) error {
		rand.New(rand.NewSource(time.Now().UnixNano()))
		switch rand.Intn(5) {
		case 0:
			return c.String(300, "REDIRECT")
		case 1:
			return c.String(400, "CLIENT ERR")
		case 2:
			return c.String(500, "SERVER ERR")
		}
		return c.String(http.StatusOK, "OK")
	})

	logging.Fatal(e.Start(":9999"))
}
```