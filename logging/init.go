package logging

import (
	"os"
	"sync"
	"time"

	"go.uber.org/zap"
	"go.uber.org/zap/zapcore"
)

var (
	atom    *zap.AtomicLevel
	logger  *zap.Logger
	sugared *zap.SugaredLogger
	once    sync.Once
)

func init() {
	once.Do(func() {
		// Define a custom time format.
		encoderCfg := zap.NewProductionEncoderConfig()
		encoderCfg.EncodeTime = func(t time.Time, enc zapcore.PrimitiveArrayEncoder) {
			enc.AppendString(t.UTC().Format(time.RFC3339))
		}
		atomInstance := zap.NewAtomicLevelAt(zap.ErrorLevel)
		loggerInstance := zap.New(zapcore.NewCore(
			zapcore.NewJSONEncoder(encoderCfg),
			zapcore.Lock(os.Stdout),
			atomInstance,
		), zap.WithCaller(true), zap.AddCallerSkip(1), zap.AddStacktrace(zap.PanicLevel))

		atom = &atomInstance
		logger = loggerInstance
		sugared = loggerInstance.Sugar()
	})
}

// SetLevel sets the logging level.
func SetLevel(levelStr string) error {
	if levelStr == "" {
		return nil
	}

	level, err := zap.ParseAtomicLevel(levelStr)
	if err != nil {
		return err
	}

	atom.SetLevel(level.Level())
	return nil
}
