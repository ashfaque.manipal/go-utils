# go-utils

This repository holds all the utility functions/middlewares used in Bobble repositories.

## API Access Logger

It offers an Echo Middleware which can be used along with Echo Handlers to enable API Access Logs.

### Usage

1. Get the package

    ```golang
    go get gitlab.com/bobble-public/backend/go-utils/apiaccesslogger
    ```

2. Import package to file

    ```golang
    import apiAccessLogger "gitlab.com/bobble-public/backend/go-utils/apiaccesslogger"
    ```

3. Use the middleware function

    ```golang
    e.Use(apiAccessLogger.Middleware)
    ```

Note: Following ENV variables are required

```bash
LOG_INGESTION_API_BASE_URL
```

## Prometheus Utilities

It offers a wrapper for golang prometheus metrics.

### Usage

1. Get the package

    ```golang
    go get gitlab.com/bobble-public/backend/go-utils/promutil
    ```

2. Import package to file

    ```golang
    import promutil "gitlab.com/bobble-public/backend/go-utils/promutil"
    ```

3. Use the required functions

    ```golang
    promutil.GetFunctionRecorder().Record("test_func", status, latency)
    promutil.GetQueueProducerRecorder().Record("test_queue", status, latency)
    promutil.GetQueueBufferRecorder().Record("test_queue", count)
    promutil.GetQueueConsumerRecorder().Record("test_queue", status, latency)
    promutil.GetQueueLatencyRecorder().Record("test_queue", p2cLatency, e2eLatency)
    promutil.GetDatabaseRecorder().Record("test", status, latency)
    promutil.GetCacheRecorder().Record("test", status, latency)
    promutil.GetExternalHTTPRecorder().Record(url, "GET", 200, latency)
    promutil.GetErrorRecorder().Record(method, code, key)
    ```

## Open cloud storage utlities

The storageservice package provides utilities for interacting with cloud storage services like Google Cloud Storage (GCS) and Amazon S3. To use this package, follow these steps:

1. Get the package

    ```golang
    go get gitlab.com/bobble-public/backend/go-utils/cloudservices/storageservice
    ```

2. Import package to file

    ```golang
    import storageservice "gitlab.com/bobble-public/backend/go-utils/cloudservices/storageservice"
    import config "gitlab.com/bobble-public/backend/go-utils/cloudservices/config"
    ```

3. Use the required functions

    ```golang

    // Set the cloud provider name from the env variable
    config.SetCloudProviderName()

    // Upload a file to cloud storage
    storageservice.NewStorage(ctx, bucketName, projectId, storageOptions).Upload(ctx, srcPath, destPath, writerOptions)

    // Download a file from cloud storage
    storageservice.NewStorage(ctx, bucketName, projectId, storageOptions).Download(ctx, srcPath, destPath)

    // Delete a file from cloud storage
    storageservice.NewStorage(ctx, bucketName, projectId, storageOptions).Delete(ctx, path)
    ```

    Note: Following ENV variables are required

    ```bash
    CLOUD_PROVIDER=GCP
    GCP_SERVICE_ACCOUNT_KEY_JSON={
    "type": "service_account",
    "project_id": "my-project",
    "private_key_id": "123abc456def789ghijklm",
    "private_key": "-----BEGIN PRIVATE KEY-----\nMIGT...",
    "client_email": "my-service-account@my-project.iam.gserviceaccount.com",
    "client_id": "12345678901234567890",
    "auth_uri": "https://accounts.google.com/o/oauth2/auth",
    "token_uri": "https://oauth2.googleapis.com/token",
    "auth_provider_x509_cert_url": "https://www.googleapis.com/oauth2/v1/certs",
    "client_x509_cert_url": "https://www.googleapis.com/robot/v1/metadata/x509/my-service-account%40my-project.iam.gserviceaccount.com"
    }


    Note that this JSON is for demonstration purposes only and should not be used as an actual service account key.

    ```

    OR

    ```bash
    CLOUD_PROVIDER=AWS
    AWS_ACCESS_KEY_ID=<access-key-id>
    AWS_SECRET_ACCESS_KEY=<secret-access-key>
    AWS_REGION=<region-name>
    ```

## Open cloud queue utlities

1. Get the package

    ```golang
    go get gitlab.com/bobble-public/backend/go-utils/cloudservices/queueservice
    ```

2. Import package to file

    ```golang
    import queueservice "gitlab.com/bobble-public/backend/go-utils/cloudservices/queueservice"
    import config "gitlab.com/bobble-public/backend/go-utils/cloudservices/config"
    ```

3. Use the required functions

    ```golang

    // Set the cloud provider name from the env variable
    config.SetCloudProviderName()

    // SendMessage pushes a new message to SQS/Pubsub
    queueservice.NewQueue(ctx, topicName, subscriptionName, queueOptions).SendMessage(ctx, message, metaData)

    // ReceiveMessage receives a message from SQS/Pubsub
    queueservice.NewQueue(ctx, topicName, subscriptionName, queueOptions).ReceiveMessage()

    // DeleteMessage deletes a message from SQS/Pubsub
    queueservice.NewQueue(ctx, topicName, subscriptionName, queueOptions).DeleteMessage(ctx, message)

    // SendMessages sends a batch of messages to SQS/Pubsub
    queueservice.NewQueue(ctx, topicName, subscriptionName, queueOptions).SendMessages(ctx, messages)

    // ReceiveMessages receives a batch of messages from SQS/Pubsub
    queueservice.NewQueue(ctx, topicName, subscriptionName, queueOptions).ReceiveMessages(maxMessages)

    // DeleteMessages deletes a batch of messages from SQS/Pubsub
    queueservice.NewQueue(ctx, topicName, subscriptionName, queueOptions).DeleteMessages(ctx, messages)
    ```

    Note: Following ENV variables are required

    #### Keyless access to GCP services with workload Identity

    ```bash
    CLOUD_PROVIDER=GCP
    ```

    OR

    #### Key based access to GCP services

    ```bash
    CLOUD_PROVIDER=GCP
    GCP_TOKEN_SOURCE=custom
    GCP_SERVICE_ACCOUNT_KEY_JSON={
    "type": "service_account",
    "project_id": "my-project",
    "private_key_id": "123abc456def789ghijklm",
    "private_key": "-----BEGIN PRIVATE KEY-----\nMIGT...",
    "client_email": "my-service-account@my-project.iam.gserviceaccount.com",
    "client_id": "12345678901234567890",
    "auth_uri": "https://accounts.google.com/o/oauth2/auth",
    "token_uri": "https://oauth2.googleapis.com/token",
    "auth_provider_x509_cert_url": "https://www.googleapis.com/oauth2/v1/certs",
    "client_x509_cert_url": "https://www.googleapis.com/robot/v1/metadata/x509/my-service-account%40my-project.iam.gserviceaccount.com"
    }


    Note that this JSON is for demonstration purposes only and should not be used as an actual service account key.

    ```

    OR

    #### Key based access to AWS services

    ```bash
    CLOUD_PROVIDER=AWS
    AWS_ACCESS_KEY_ID=<access-key-id>
    AWS_SECRET_ACCESS_KEY=<secret-access-key>
    AWS_REGION=<region-name>
    ```

## Open cloud bigquery utlity

The bigqueryservice package provides utility for interacting with GCP bigquery. To use this package, follow these steps:

1. Get the package

    ```golang
    go get gitlab.com/bobble-public/backend/go-utils/cloudservices/bigqueryservice
    ```

2. Import package to file

    ```golang
    import bigqueryservice "gitlab.com/bobble-public/backend/go-utils/cloudservices/bigqueryservice"
    ```

3. Use the required functions

    ```golang
    // Initialize BigQuery client
    bigqueryservice.NewClient(ctx, projectId)
    ```

    Note: Following ENV variables are required

    ```bash
    GCP_TOKEN_SOURCE=custom
    GCP_PROJECT_ID=project-id
    GCP_SERVICE_ACCOUNT_KEY_JSON={
    "type": "service_account",
    "project_id": "my-project",
    "private_key_id": "123abc456def789ghijklm",
    "private_key": "-----BEGIN PRIVATE KEY-----\nMIGT...",
    "client_email": "my-service-account@my-project.iam.gserviceaccount.com",
    "client_id": "12345678901234567890",
    "auth_uri": "https://accounts.google.com/o/oauth2/auth",
    "token_uri": "https://oauth2.googleapis.com/token",
    "auth_provider_x509_cert_url": "https://www.googleapis.com/oauth2/v1/certs",
    "client_x509_cert_url": "https://www.googleapis.com/robot/v1/metadata/x509/my-service-account%40my-project.iam.gserviceaccount.com"
    }

    Note that this JSON is for demonstration purposes only and should not be used as an actual service account key.
    ```
