package httpclient

import (
	"net/http"
)

type httpClient struct {
	client *http.Client
}

// HTTPClient represents the interface containing function declarations to operate on the http client
type HTTPClient interface {
	// Do sends an HTTP request and returns an HTTP response, following policy (such as redirects, cookies, auth) as configured on the client.
	Do(request *http.Request) (*http.Response, error)
}

// NewHTTPClient creates an object that represent the HTTPClient interface
func NewHTTPClient(client *http.Client) HTTPClient {
	return &httpClient{
		client: client,
	}
}

// Do is a wrapper around the http client Do function
func (h *httpClient) Do(request *http.Request) (*http.Response, error) {
	return h.client.Do(request)
}
