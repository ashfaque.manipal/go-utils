package promutil

import (
	"sync"

	"github.com/prometheus/client_golang/prometheus"
)

var queueConsumerRecorderInstance *queueConsumerRecorder
var queueConsumerRecorderSingleton sync.Once

type queueConsumerRecorder struct {
	latency *prometheus.HistogramVec
	count   *prometheus.CounterVec
}

type QueueConsumerRecorder interface {
	Record(name string, status string, latency float64)
}

func GetQueueConsumerRecorder() QueueConsumerRecorder {
	queueConsumerRecorderSingleton.Do(func() {
		latency := prometheus.NewHistogramVec(
			prometheus.HistogramOpts{
				Name:    "queue_consumer_duration_seconds",
				Buckets: DefBuckets,
			}, []string{"name", "status"})
		count := prometheus.NewCounterVec(
			prometheus.CounterOpts{
				Name: "queue_consumer_count_total",
			}, []string{"name", "status"})

		prometheus.MustRegister(latency)
		prometheus.MustRegister(count)

		queueConsumerRecorderInstance = &queueConsumerRecorder{
			latency: latency,
			count:   count,
		}
	})

	return queueConsumerRecorderInstance
}

func (queueConsumerRecorder *queueConsumerRecorder) Record(name string, status string, latency float64) {
	queueConsumerRecorder.count.WithLabelValues(name, status).Inc()
	queueConsumerRecorder.latency.WithLabelValues(name, status).Observe(latency)
}
