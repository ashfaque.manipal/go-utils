package promutil

import (
	"sync"

	"github.com/prometheus/client_golang/prometheus"
)

var databaseRecorderInstance *databaseRecorder
var databaseRecorderSingleton sync.Once

type databaseRecorder struct {
	latency *prometheus.HistogramVec
	count   *prometheus.CounterVec
}

type DatabaseRecorder interface {
	Record(key string, status string, latency float64)
}

func GetDatabaseRecorder() DatabaseRecorder {
	databaseRecorderSingleton.Do(func() {
		latency := prometheus.NewHistogramVec(
			prometheus.HistogramOpts{
				Name:    "database_duration_seconds",
				Buckets: DefBuckets,
			}, []string{"key", "status"})
		count := prometheus.NewCounterVec(
			prometheus.CounterOpts{
				Name: "database_calls_total",
			}, []string{"key", "status"})

		prometheus.MustRegister(latency)
		prometheus.MustRegister(count)

		databaseRecorderInstance = &databaseRecorder{
			latency: latency,
			count:   count,
		}
	})

	return databaseRecorderInstance
}

func (databaseRecorder *databaseRecorder) Record(key string, status string, latency float64) {
	databaseRecorder.count.WithLabelValues(key, status).Inc()
	databaseRecorder.latency.WithLabelValues(key, status).Observe(latency)
}
