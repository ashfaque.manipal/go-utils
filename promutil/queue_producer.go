package promutil

import (
	"sync"

	"github.com/prometheus/client_golang/prometheus"
)

var queueProducerRecorderInstance *queueProducerRecorder
var queueProducerRecorderSingleton sync.Once

type queueProducerRecorder struct {
	latency *prometheus.HistogramVec
	count   *prometheus.CounterVec
}

type QueueProducerRecorder interface {
	Record(name string, status string, latency float64)
}

func GetQueueProducerRecorder() QueueProducerRecorder {
	queueProducerRecorderSingleton.Do(func() {
		latency := prometheus.NewHistogramVec(
			prometheus.HistogramOpts{
				Name:    "queue_producer_duration_seconds",
				Buckets: DefBuckets,
			}, []string{"name", "status"})
		count := prometheus.NewCounterVec(
			prometheus.CounterOpts{
				Name: "queue_producer_count_total",
			}, []string{"name", "status"})

		prometheus.MustRegister(latency)
		prometheus.MustRegister(count)

		queueProducerRecorderInstance = &queueProducerRecorder{
			latency: latency,
			count:   count,
		}
	})

	return queueProducerRecorderInstance
}

func (queueProducerRecorder *queueProducerRecorder) Record(name string, status string, latency float64) {
	queueProducerRecorderInstance.count.WithLabelValues(name, status).Inc()
	queueProducerRecorderInstance.latency.WithLabelValues(name, status).Observe(latency)
}
