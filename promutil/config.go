package promutil

// DefBuckets is the default buckets used for histograms.
var DefBuckets = []float64{.005, .01, .025, .05, .1, .25, .5, 1, 2.5, 5, 10, 20, 30, 60, 120}
