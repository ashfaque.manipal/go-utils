package promutil

import (
	"sync"

	"github.com/prometheus/client_golang/prometheus"
)

var cacheRecorderInstance *cacheRecorder
var cacheRecorderSingleton sync.Once

type cacheRecorder struct {
	latency *prometheus.HistogramVec
	count   *prometheus.CounterVec
}

type CacheRecorder interface {
	Record(key string, status string, latency float64)
}

func GetCacheRecorder() CacheRecorder {
	cacheRecorderSingleton.Do(func() {
		latency := prometheus.NewHistogramVec(
			prometheus.HistogramOpts{
				Name:    "cache_duration_seconds",
				Buckets: DefBuckets,
			}, []string{"key", "status"})
		count := prometheus.NewCounterVec(
			prometheus.CounterOpts{
				Name: "cache_calls_total",
			}, []string{"key", "status"})

		prometheus.MustRegister(latency)
		prometheus.MustRegister(count)

		cacheRecorderInstance = &cacheRecorder{
			latency: latency,
			count:   count,
		}
	})

	return cacheRecorderInstance
}

func (cacheRecorder *cacheRecorder) Record(key string, status string, latency float64) {
	cacheRecorder.count.WithLabelValues(key, status).Inc()
	cacheRecorder.latency.WithLabelValues(key, status).Observe(latency)
}
